#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <iostream>                  // for std::cout
#include <utility>                   // for std::pair
#include <algorithm>                 // for std::for_each
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/strong_components.hpp>
#include <vector>
#include <map>
#include <cstdlib>
#include <algorithm>

#include <random>
#include <math.h>
#include "gnuplot_i.hpp"

using namespace std;
using Eigen::MatrixXd;
using Eigen::EigenSolver;
using namespace Eigen;
using namespace boost;

typedef adjacency_list<vecS, vecS, bidirectionalS> Graph;
typedef property_map<Graph, vertex_index_t>::type IndexMap;
typedef graph_traits<Graph>::vertex_iterator vertex_iter;
typedef graph_traits<Graph>::edge_iterator edge_iter;
ofstream outputFileStream;

MatrixXd readConfig(MatrixXd &mat, int &steps, int &runs, vector<double> &C0, char *fileHandle)
{
    ifstream input(fileHandle);
    int r, c;
    //read row and column
    input >> r;
    c = r;
    mat.resize(r, c);
    //    MatrixXd mat(r, c);
    //read matrix
    for (int i = 0; i < r; i++)
        for (int j = 0; j < c; j++)
        {
            float v;
            input >> v;
            mat(i, j) = v;
        }

    outputFileStream << "Number of colors: " << r << endl << endl;
    outputFileStream << "Replacement Matrix" << endl;

    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            outputFileStream << mat(i, j) << " ";
        }
        outputFileStream << endl;
    }
    outputFileStream << endl;
    input.ignore(500, '#');
    input >> steps;
    input >> runs;
    input.ignore(500, '#');

    outputFileStream << "Initial Color Configuration" << endl;

    for (int j = 0; j < r; j++)
    {
        double n;
        input >> n;
        outputFileStream<<n<<" ";
        C0.push_back(n);
    }
    outputFileStream<<endl;
    outputFileStream << endl;
    outputFileStream<<"Steps : "<<steps<<endl;
    outputFileStream<<"Runs : "<<runs<<endl;
    outputFileStream << endl;

    return mat;
}

int reverseEdge(int i, int j, map<int, vector<int> > componentGroups, Graph g )
{
    for (auto a = componentGroups[i].cbegin(); a != componentGroups[i].cend(); a++)
    {
        for (auto b = componentGroups[j].cbegin(); b != componentGroups[j].cend(); b++)
        {
            if (edge(*b, *a, g).second)
                return 1;
        }
    }
    return 0;
}
inline void add(vector<double> &C, MatrixXd R, int len)
{
    for (int i = 0; i < len; i++)
    {
        C[i] = C[i] + R(i);
    }
}
void plot(vector<vector<double>> C_list, vector<double> alphas, vector<double> betas, int steps, int matsize)
{
    
    for (int d = 0; d < matsize; d++)
    {
        double *pointsx = new double[steps];
        double *pointsy = new double[steps];
        double *histodata = new double[C_list.size() / steps];
        double max_histodata=0.0;
        gnuplot_ctrl *h1;
        h1 = gnuplot_init();
        gnuplot_cmd(h1, "set terminal png size 1024,768");
        gnuplot_cmd(h1, "set output \"plot%d.png\" ", (d + 1));
        gnuplot_cmd(h1, "set multiplot");
        gnuplot_cmd(h1, "set xrange [%d:%d]", 1, steps);
        gnuplot_cmd(h1, "set yrange [%d:%d]", 0, 1);
        // gnuplot_cmd(h1, "set autoscale");
        gnuplot_cmd(h1, "unset ytics");
        gnuplot_cmd(h1, "unset xtics");
        // gnuplot_cmd(h1, "set ytics 0.5");
        //gnuplot_cmd(h1, "set xtics 100");
        gnuplot_cmd(h1, "set origin 0,0");
        gnuplot_cmd(h1, "set size 1,1");
        gnuplot_setstyle(h1, "lines");
        for (int n = 0; n < C_list.size(); n++)
        {
            vector<double> C = C_list[n];
            int m = n % steps;
            double output = C[d] / (pow((m + 1), alphas[d]) * (pow(log(m + 1), betas[d])));
            pointsx[m] = m + 1;
            pointsy[m] = output;
            //color<<(m+1)<<' '<<output<<endl;
            if (m == (steps - 1))
            {
                //histodata<<output<<endl;
                histodata[n / steps] = output;
                if(output>max_histodata)
                max_histodata=output;
                gnuplot_cmd(h1, "set xrange [%d:%d]", 1, steps);
                gnuplot_cmd(h1, "set yrange [%d:%d]", 0, 1);
                // gnuplot_cmd(h1, "set autoscale");
                gnuplot_cmd(h1, "set ytics 0.1");
                gnuplot_cmd(h1, "set xtics 100");
                gnuplot_cmd(h1, "set origin 0,0");
                gnuplot_cmd(h1, "set size 1,1");
                gnuplot_setstyle(h1, "lines");
                gnuplot_plot1d_var2v(h1, pointsx, pointsy, steps, "" );
                gnuplot_cmd(h1, "unset ytics");
                gnuplot_cmd(h1, "unset xtics");
                gnuplot_cmd(h1, "unset xrange");
                gnuplot_cmd(h1, "unset yrange");
            }
        }
        ofstream tmp("tmp.txt");
        for (int f = 0; f < C_list.size() / steps; f++)
            tmp << histodata[f] << endl;
        tmp.close();
        
        gnuplot_ctrl *histogram;
        histogram = gnuplot_init();
        gnuplot_setstyle(histogram, "data histogram");
        gnuplot_cmd(histogram, "set terminal png size 1024,768");
        gnuplot_cmd(histogram, "set output \"histogram%d.png\" ", (d + 1));
        gnuplot_cmd(histogram, "set autoscale");
        gnuplot_cmd(histogram, "set xrange [%d:%d]",0,1);
        //gnuplot_cmd(histogram, "set yrange [%d:%d]",0,(int)(max_histodata+10.0));
        gnuplot_cmd(histogram, "set xtics 0.1");

        //gnuplot_cmd(histogram, "set ytics %d",(int)(max_histodata+10.0)/10);
        gnuplot_cmd(histogram, "bin_width=0.01");
        gnuplot_cmd(histogram, "set style fill solid 1.0");
        gnuplot_cmd(histogram, "set boxwidth 0.005 absolute");
        gnuplot_cmd(histogram, "bin_number(x)=floor(x/bin_width)");
        gnuplot_cmd(histogram, "r(x)=bin_width*(bin_number(x))");
        gnuplot_cmd(histogram, (char *)"plot \"tmp.txt\" using (r($1)):(1) smooth frequency with boxes");
        //gnuplot_plot1d_var1(histogram, histodata, C_list.size() / steps, "");
        gnuplot_close (histogram);

        gnuplot_resetplot(h1);

    }

}
void simulate(int matsize, MatrixXd matrix, vector<double> alphas, vector<double> betas, int steps, int runs, vector<double> C0)
{
    vector<vector<double>> C_list;
    double csum, csumold;
    for (int x = 0; x < C0.size(); x++)
        csum += C0[x];
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0.0, 1.0);
    for (int r = 1; r <= runs; r++)
    {
        double csum = 0.0;
        vector<double> Cx(C0);
        for (int s = 1; s <= steps; s++)
        {
            csum = (s - 1) + csumold;
            double Un = distribution(generator);
            double g = Un * csum;
            double chksum = 0;
            int i = 0;
            while (g > chksum)
            {
                chksum += Cx[i];
                i += 1;
            }
            int Xn;
            if (i == 0)
            {
                Xn = i;
            }
            else
                Xn = i - 1;
            add(Cx, matrix.block(Xn,0,1,matrix.cols()), matsize);
            C_list.push_back(Cx);

        }
    }
    // outputFileStream << "C_list done" << endl;
    /*
    for(int d=0;d<matsize;d++){

            char fname[100];
            sprintf(fname,"histodata%d.txt",(d+1));
            ofstream histodata;
            histodata.open(fname);
            sprintf(fname,"color%d.txt",(d+1));
            ofstream color(fname);
            for(int n=0;n<C_list.size();n++){
                vector<double> C=C_list[n];
                int m=n%steps;
                double output=C[d]/(pow((m+1),alphas[d])*(pow(log(m+1),betas[d])));
                color<<(m+1)<<' '<<output<<endl;
                if(m==(steps-1))
                    histodata<<output<<endl;

            }
            color.close();
    }
    */
    plot(C_list, alphas, betas, steps, matsize);

    /*
    for (std::vector<vector<double>>::iterator it = C_list.begin() ; it != C_list.end(); ++it){
        for(int i=0;i<matsize;i++)
            cout<<(*it)[i]<<' ';
        cout<<endl;

    }*/
}

int main(int argc, char *argv[])
{
    pair<edge_iter, edge_iter> ep;
    pair<vertex_iter, vertex_iter> vp;
    int steps, runs;
    vector<double> C0;
    MatrixXd mat;
    char *outputFile;
    if (argv[1] == NULL) {
        outputFileStream << "Missing Replacement Matrix and Input configurations" << endl;
        exit (1);
    }
    if(argc>2)
    {
        outputFile=argv[2];
    }
    else
        outputFile="output";

    //open output file for writing...
    outputFileStream.open(outputFile,ofstream::out);
    //read the configuration from file...
    readConfig(mat, steps, runs, C0, argv[1]);

    //read input matrix
    Graph directedGraph(mat.rows());
    IndexMap index = get(vertex_index, directedGraph);
    //add vertices

    //add edges
    for (int i = 0; i < mat.rows(); i++)
    {
        for (int j = 0; j < mat.cols(); j++)
        {
            if (mat(i, j) > 0)
            {
                add_edge(i, j, directedGraph);
            }
        }
    }

    outputFileStream << "Detail of graph generated" << endl;
    outputFileStream << "Vertices" << endl;
    for (vp = vertices(directedGraph); vp.first != vp.second; ++vp.first)
        outputFileStream << index[*vp.first] <<  " ";
    outputFileStream << endl;

    outputFileStream << "Edges" << endl;
    for (ep = edges(directedGraph); ep.first != ep.second; ++ep.first)
    {
        outputFileStream << index[source(*ep.first, directedGraph)] << "->" << index[target(*ep.first, directedGraph)] << endl;
    }
    outputFileStream << endl;

    vector<int> component(num_vertices(directedGraph));
    int num = strong_components(directedGraph, make_iterator_property_map(component.begin(), get(vertex_index, directedGraph)));
    map<int, vector<int> > componentGroups;

    for (int i = 0; i < component.size(); i++)
        componentGroups[component[i]].push_back(i);
    //created component Map
    outputFileStream << "Component Map" << endl;
    for (auto it = componentGroups.cbegin(); it != componentGroups.cend(); it++)
    {
        outputFileStream << (*it).first << "->";
        for (auto it2 = (*it).second.cbegin(); it2 != (*it).second.cend(); it2++)
            outputFileStream << (*it2) << " ";
        outputFileStream << endl;
    }

    vector<int> componentOrder(componentGroups.size());
    iota(componentOrder.begin(), componentOrder.end(), 0);

    //sort to get the order
    for (int i = 0; i < componentOrder.size() - 1; i++)
        for (int j = i + 1; j < componentOrder.size(); j++)
        {
            if (reverseEdge(componentOrder[i], componentOrder[j], componentGroups, directedGraph))
            {
                int x = componentOrder[i];
                componentOrder[i] = componentOrder[j];
                componentOrder[j] = x;
            }

        }
    outputFileStream << "Component Order" << endl;
    for (auto it = componentOrder.cbegin(); it != componentOrder.cend(); it++)
        outputFileStream << *it << " ";
    outputFileStream << endl;
    vector<int> neworder;

    for (auto it = componentOrder.cbegin(); it != componentOrder.cend(); it++)
        for (auto b = componentGroups[*it].cbegin(); b != componentGroups[*it].cend(); b++)
            neworder.push_back(*b);

    //create new reordered matrix...
    MatrixXd newmatrix(mat.rows(), mat.rows());
    for (int i = 0; i < mat.rows(); i++)
        for (int j = 0; j < mat.cols(); j++)
            newmatrix(i, j) = mat(neworder[i], neworder[j]);
    outputFileStream << "New matrix" << endl;
    outputFileStream << newmatrix << endl;

    //find eigen values
    int start = 0;
    vector<double> eigens;
    for (int i = 0; i < componentGroups.size(); i++)
    {
        MatrixXd blockmat = newmatrix.block(start, start, componentGroups[componentOrder[i]].size(), componentGroups[componentOrder[i]].size());
        EigenSolver<MatrixXd> es(blockmat);
        //find max eigenvalue!
        auto eigenvals = es.eigenvalues();
        auto max_eigen = es.eigenvalues()[0];
        for (int j = 1; j < eigenvals.size(); j++)
        {
            if (abs(eigenvals[j]) > abs(max_eigen))
                max_eigen = eigenvals[j];

        }
        double maxEigenValue = abs(max_eigen); //hope this works...
        //store according to componentOrder
        // outputFileStream<<"Max eigen : "<<maxEigenValue<<endl;
        eigens.push_back(maxEigenValue);
        start += componentGroups[componentOrder[i]].size();
    }
    //print eigens
    outputFileStream << endl << "Perron-Frobenius Eigenvalues :" << endl;
    for (int i = 0; i < eigens.size(); i++)
        outputFileStream << eigens[i] << endl;
    //find alphas and betas
    vector<double> alphas;
    vector<double> betas;
    alphas.push_back(eigens[0]);
    betas.push_back(0.0);
    int rx = 0;
    int cx = componentGroups[componentOrder[0]].size();
    for (int i = 1; i < componentGroups.size(); i++)
    {
        double a_max = -1;
        double b_max = 0;
        rx = 0;
        //int rx = 0;
        //int cx =
        for (int j = 0; j < i; j++)
        {
            MatrixXd matx = newmatrix.block(rx, cx, componentGroups[componentOrder[j]].size(), componentGroups[componentOrder[i]].size());
            rx += componentGroups[componentOrder[j]].size();
            if (!matx.isZero() && (alphas[j] > a_max || (alphas[j] == a_max && betas[j] > b_max)))
            {
                a_max = alphas[j];
                b_max = betas[j];

            }
        }
        cx += componentGroups[componentOrder[i]].size();
        if (eigens[i] > a_max)
        {
            alphas.push_back(eigens[i]);
            betas.push_back(0.0);
        }
        else if (eigens[i] == a_max)
        {
            alphas.push_back(eigens[i]);
            betas.push_back(b_max + 1);
        }
        else
        {
            alphas.push_back(a_max);
            betas.push_back(b_max);
        }
    }
    outputFileStream << endl << "Alphas & Betas" << endl;
    for (int i = 0; i < alphas.size(); i++)
        outputFileStream << "(" << alphas[i] << "," << betas[i] << ")" << endl;

    vector<double> alpha_list(mat.rows(), 0);
    vector<double> beta_list(mat.rows(), 0);
    for (int i = 0; i < componentOrder.size(); i++)
    {
        for (auto it = componentGroups[componentOrder[i]].cbegin(); it != componentGroups[componentOrder[i]].cend(); it++)
        {
            alpha_list[*it] = alphas[i];
            beta_list[*it] = betas[i];
        }
    }

    ////////////////////simulate/////////////////////////////////
    simulate(mat.rows(), newmatrix, alpha_list, beta_list, steps, runs, C0);
    ////////////////////////////////////////////////////////////
}
