Urn Model Simulator
====================================================
* Download the code from bitBucket
* Go inside gnuPlot
* make -- this will build libgnuPlot.a
* come out of gnuplot and enter main directory
* make -- this will build the urnDemo executable
=============================================================================================
How to run:
<path-to-urnDemo>/urnDemo inputConfigFile [outputFile] >& /dev/null
=============================================================================================
Sample Input Config File Format:
<Color Count>
<...matrix data (row major format)...
.........
.......>
<steps> <runs>
C0#
<Initial color configuration vector>

Important points:
1. Text between <> are to be replaced by actual inputs.
2. Color Count refers to the number of colors
3. The number of of rows and columns are both equal to Color count
4. The initial color configuration vector size should match the color count
5. Steps is number of draws made from an urn, while the run is the number of times an urn 
   is simulated
6. Check out some sample input file provided inside Runs directory
   (sampleInput1, sampleInput2, etc.) provided in this directory.
7. Do not put any blank lines in the input file.
8. Run the urnDemo as above

=============================================================================================

Output Format:
For a DxD input matrix and D sized input color vector:
1. D line graphs (plot1.png, plot2.png, etc.) are produced(one for each color). 
   It plots the change of the weight of the colour in each run through S steps(x-axis).
2. D histograms (histogram1.png, histogram2.png, etc.) one for each color. 
   Plots the frequency distribution of each weight of a color. Bin size=0.01.
3. An output file which lists the following (in order):
	C0
	Input Matrix
	Steps
	Runs
	Connected Components in graph and their relative ordering
	New Configuration Matrix(after reordering)
	Perron-Frobenius eigenvalues for each component
	Alpha and betas for each component
4. All colors in a component have the same growth rate of the form n^alpha (log n)^beta. 
   The pair (alpha, beta) for each component is given above.

=============================================================================================
